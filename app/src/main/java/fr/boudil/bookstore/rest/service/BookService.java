package fr.boudil.bookstore.rest.service;

import fr.boudil.bookstore.rest.model.Book;
import fr.boudil.bookstore.rest.model.Offers;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by rachid on 27/10/2015.
 */
public interface BookService {
    String URL_BASE = "http://henri-potier.xebia.fr/";
    String URL_BOOKS = "/books";
    String URL_OFFERS = "/books/{isbn}/commercialOffers";
    String PARAM_ISBN = "isbn";

    @GET(URL_BOOKS)
    public void getBooks(Callback<Book[]> callback);

    @GET(URL_OFFERS)
    public void getOffers(@Path(PARAM_ISBN) String isbn, Callback<Offers> callback);
}

