package fr.boudil.bookstore.rest.model;

/**
 * Created by rachid on 27/10/2015.
 */
public class Book {
    private String isbn;
    private String title;
    private Integer price;
    private String cover;

    public Book() {
        this.cover = "";
        this.isbn = "";
        this.price = 0;
        this.title = "";
    }

    public Book(String cover, String isbn, Integer price, String title) {
        this.cover = cover;
        this.isbn = isbn;
        this.price = price;
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
