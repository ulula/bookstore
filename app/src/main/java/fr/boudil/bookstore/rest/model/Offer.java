package fr.boudil.bookstore.rest.model;

/**
 * Created by rachid on 27/10/2015.
 */
public class Offer {
    private String type;
    private Integer value;
    private Integer sliceValue;

    public Offer(Integer sliceValue, String type, Integer value) {
        this.sliceValue = sliceValue;
        this.type = type;
        this.value = value;
    }
}
