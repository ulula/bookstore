package fr.boudil.bookstore;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.boudil.bookstore.adapter.BookListAdapter;
import fr.boudil.bookstore.rest.model.Book;
import fr.boudil.bookstore.rest.service.BookService;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends ListFragment {
    private static final String TAG = "MainActivityFragment";
    // callback pour le listing du catalogue
    private Callback<Book[]> mRefreshCallback = new Callback<Book[]>() {
        @Override
        public void success(Book[] books, Response response) {
            BookListAdapter adapter = new BookListAdapter(getActivity(), books);
            setListAdapter(adapter);
            hideProgress();
        }

        @Override
        public void failure(RetrofitError error) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "refresh error :", error);
            hideProgress();
        }
    };

    @Bind(R.id.progress)
    ProgressBar progressBar;
    @Bind(android.R.id.list)
    ListView listView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        refresh();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh() {
        showProgress();
        BookService service = Application.getBookService();
        service.getBooks(mRefreshCallback);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
    }
    private void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.VISIBLE);
    }
}
