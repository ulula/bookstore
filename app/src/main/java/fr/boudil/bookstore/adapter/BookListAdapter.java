package fr.boudil.bookstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.Bind;
import fr.boudil.bookstore.R;
import fr.boudil.bookstore.rest.model.Book;

/**
 * Created by rachid on 27/10/2015.
 */
public class BookListAdapter extends ArrayAdapter<Book> {
    private final LayoutInflater mInflater;

    public BookListAdapter(Context context, Book[] books) {
        super(context, R.layout.item_book, books);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_book, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        // initialise la vue
        Book book = this.getItem(position);
        holder.title.setText(book.getTitle());
        holder.price.setText(book.getPrice() + "€");

        // chargement asynchrone de la couverture du livre
        Picasso.with(getContext())
                .load(book.getCover())
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(holder.cover);

        return view;
    }

    class ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.price)
        TextView price;
        @Bind(R.id.cover)
        ImageView cover;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
