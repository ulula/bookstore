package fr.boudil.bookstore;

import butterknife.ButterKnife;
import fr.boudil.bookstore.rest.service.BookService;
import retrofit.RestAdapter;

/**
 * Created by rachid on 27/10/2015.
 */
public class Application extends android.app.Application {
    private static BookService mBookService;

    @Override
    public void onCreate() {
        super.onCreate();
        ButterKnife.setDebug(BuildConfig.DEBUG);
    }

    public static BookService getBookService() {
        if(mBookService == null ) {
            // create REST service
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(BookService.URL_BASE)
                    .build();

            mBookService = restAdapter.create(BookService.class);
        }
        return mBookService;
    }
}
